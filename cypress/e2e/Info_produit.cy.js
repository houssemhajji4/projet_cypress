const { filePathDe, filePathAu } = require('../support/Variables');
const { getDataFromExcel } = require('../support/Fonctions');


describe("Parcours amazon d'un achat de nouveau produit jusqu'à le paiement", () => {

    let proData;
  
    before(() => {
      
      // Effacer tous les cookies  
      cy.clearAllCookies();
      // Effacer tout le stockage local
      cy.clearAllLocalStorage();
  
      cy.task('readExcelFile', filePathAu).then((data) => {
      // Vérifie que les données ne sont pas nulles
        const sheet = data.sheet;
        proData = getDataFromExcel(sheet);

       });
    });
  
    it('Se connecter à Amazon', () => {  
      // Visiter le site Amazon
      cy.visit("https://www.amazon.com/");
    });

    it('Authentifier le compte Amazon', () => {
      // cliquer sur sign in 
      cy.get('#nav-link-accountList').click();
      // Saisir l'adresse email
      cy.get('#ap_email').type(`${proData.login}`);
      // Cliquer sur le bouton "Continuer"
      cy.get('.a-button-inner > #continue').click();
      // Saisir le mot de passe
      cy.get('#ap_password').type(`${proData.mdp}`);
      // Cliquer sur le bouton de connexion
      cy.get('#signInSubmit').click();
      //Verifier le nom d'utilisateur
      cy.get('#glow-ingress-line1').should('be.visible').and('contain', 'Houssem');
       });
      
    it('Chercher un produit', () => {  
        // Saisir le produit "Acer" dans la barre de recherche
      cy.get('#twotabsearchtextbox').type(`${proData.produit}`);
      //Cliquer sur le bouton de recherche
      cy.get('#nav-search-submit-button').click()
      //Cliquer sur le 1ere produit 
      cy.get('div[data-cel-widget="search_result_1"] h2 a').click(); 
        // Créez un objet pour stocker les données extraites
          const prodData = {
            Brand: '',
            Series: '',
            Model: '',
            System: '',
            Color: '',
            RAM: '',
            HardDrive: '',
            GlobalRating: '',
            CustomerReview5Star: ''
          };
          // Extraire les valeurs de la table
      cy.get('#productDetails_techSpec_section_1').within(() => {
        cy.get('tr').each(($row) => {
            const key = $row.find('th').text().trim();
            const value = $row.find('td').text().trim();
          
            if (key === 'RAM') {
              prodData.RAM = value;
              } else if (key === 'Hard Drive') {
                prodData.HardDrive = value;
              }
            });
          });
        
      cy.get('#productDetails_techSpec_section_2').within(() => { 
            cy.get('tr').each(($row) => {
              const key = $row.find('th').text().trim();
              const value = $row.find('td').text().trim();
          
              if (key === 'Brand') {
                prodData.Brand = value;
              } else if (key === 'Series') {
                prodData.Series = value;
              } else if (key === 'Item model number') {
                prodData.Model = value;
              } else if (key === 'Operating System') {
                prodData.System = value;
              } else if (key === 'Color') {
                prodData.Color = value;
              }
            });
          });
          
          // Extraire les valeurs de l'évaluation globale
      cy.get('#cm_cr_dp_d_rating_histogram').within(() => {
        cy.get('span[data-hook="total-review-count"]').invoke('text').then((text) => {
              prodData.GlobalRating = text.match(/\d+/)[0];
            });
          
      cy.get('tr.a-histogram-row').eq(0).within(() => {
        cy.get('a[aria-label*="5 stars"]').invoke('text').then((text) => {
                prodData.CustomerReview5Star = text.trim();
              });
            });
          }).then(() => {
            // Préparer les données pour le fichier Excel
            const excelData = [
              ['Brand:', prodData.Brand],
              ['Series:', prodData.Series], 
              ['Model:', prodData.Model], 
              ['System:', prodData.System], 
              ['Color:', prodData.Color], 
              ['RAM:', prodData.RAM], 
              ['Hard Drive:', prodData.HardDrive], 
              ['Global Rating:', prodData.GlobalRating], 
              ['Customer Review for 5 Star:', prodData.CustomerReview5Star] 
            ];
      //Vérifier si le fichier existe déjà
      cy.task('checkFileExists', filePathDe).then(fileExists => {
        if (fileExists) {
          // Supprimer le fichier existant s'il existe
         return cy.task('deleteFile', filePathDe);
        }
      }).then(() => {
      // Attendre 2 secondes
      cy.wait(2000);
        // Créer le fichier Excel
        return cy.task('createExcelFile', { data: excelData, filepath: filePathDe});
      })
      });    
    
    });
  })