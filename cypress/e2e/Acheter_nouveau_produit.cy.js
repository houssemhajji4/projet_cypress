const { amazonUrl } = require('../support/Variables');
const { filePathAu } = require('../support/Variables');
const { getDataFromExcel, verifierSommeTotal, verifierProduit} = require('../support/Fonctions');
import 'cypress-iframe';

describe("Parcours amazon d'un achat de nouveau produit jusqu'à le paiement", () => {

  let proData;

  before(() => {
    // Effacer tous les cookies  
    cy.clearAllCookies();
    // Effacer tout le stockage local
    cy.clearAllLocalStorage();

    cy.task('readExcelFile', filePathAu).then((data) => {
    
      // Vérifie que les données ne sont pas nulles
      const sheet = data.sheet;
      proData = getDataFromExcel(sheet);
      cy.log('la valeur de proData : ' + proData);
      expect(proData.produit).to.exist;
      expect(proData.ncProd).to.exist;
    
   });
  });

  it('Se connecter à Amazon', () => {  
    // Visiter le site Amazon
    cy.visit(amazonUrl);
  });

  it('Authentifier Amazon', () => {
    // cliquer sur sign in 
    cy.get('#nav-link-accountList').click();

    // Saisir l'adresse email
    cy.get('#ap_email').type(`${proData.login}`);
  
    // Cliquer sur le bouton "Continuer"
    cy.get('.a-button-inner > #continue').click();
    
    // Saisir le mot de passe
    cy.get('#ap_password').type(`${proData.mdp}`);

    // Cliquer sur le bouton de connexion
    cy.get('#signInSubmit').click();
    //Verifier le nom d'utilisateur
    cy.get('#glow-ingress-line1').should('be.visible').and('contain', 'Houssem');

  
  });

  it('Chercher un produit', () => {  
    // Saisir le produit "Acer" dans la barre de recherche
    cy.get('#twotabsearchtextbox').type(`${proData.produit}`);
    //Cliquer sur le bouton de recherche
    cy.get('#nav-search-submit-button').click()
    //Filtrer Brand: Acer
    cy.get('li[id="p_123/247341"]').find('input[type="checkbox"]').check({ force: true });
  
  });

  it('Ajouter le produit au panier',( )=>{
      //Ajouter le produit au panier
    cy.get('#a-autoid-1-announce').click()

  });

  
  it('Consulter panier', () => {

    // Cliquer sur le Panier
    cy.get('#nav-cart-count-container').click();

    //Fonction pour verifier le nombre de produit a acheter
    verifierProduit(proData.nbrProd);
    //Taper quantite produit
    cy.get('#a-autoid-1-announce').click()
    cy.get(`#quantity_${proData.qte}`).click();
    //Fonction pour verifier la somme total de produits
    verifierSommeTotal(`${proData.qte}`);

  });
  
  it('Procéder le paiement',()=>{
    // Cliquer pour procéder au paiement
    cy.get('#sc-buy-box-ptc-button > .a-button-inner > .a-button-input').click()
    //Ajouter la carte 
    cy.get('.a-row > .apx-add-pm-trigger-common-image').click()
   // Attendre que l'iframe soit disponible
    cy.get('iframe.apx-secure-iframe').should('be.visible').then(($iframe) => {
    // Accéder à l'iframe
    const body = $iframe.contents().find('body');
    // Saisir le numéro de la carte
    cy.wrap(body).find('input[name="addCreditCardNumber"]').type('1234567812345678');
    // Saisir le nom de la carte
    cy.wrap(body).find('input[name="ppw-accountHolderName"]').type('visa');
      // Expiration date (3/2026)
    cy.wrap(body).find('.a-button-text.a-declarative').first().click();
    cy.wrap(body).find('a.a-dropdown-link[data-value*="3"]').click();
    cy.wrap(body).find('.a-button-text.a-declarative').last().click();
    cy.wrap(body).find('a.a-dropdown-link[data-value*="2026"]').click();
    // Code de sécurité
    cy.wrap(body).find('input[name="addCreditCardVerificationNumber"]').type('0213');
    // Valider carte
    cy.wrap(body).find('input[name="ppw-widgetEvent:AddCreditCardEvent"]').click();
    cy.wrap(body).find('div.a-box-inner.a-alert-container').should('be.visible')
        .and('contain', 'There was a problem.');

});


 })
})
