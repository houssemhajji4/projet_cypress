const XLSX = require('xlsx');

function getValueFromCell(sheet, cellIndex) {
    const cell = sheet[cellIndex];
    return cell ? cell.v : undefined; // .v contient la valeur brute de la cellule
  }

function getDataFromExcel(sheet) {
    const login = getValueFromCell(sheet, 'A3');
    const mdp = getValueFromCell(sheet, 'B3');
    const nbrProd=getValueFromCell(sheet,'C3')
    const produit = getValueFromCell(sheet, 'D3');
    const qte = getValueFromCell(sheet, 'E3');
    const ncProd = getValueFromCell(sheet, 'F3');

    return {
        login, 
        mdp,
        nbrProd,
        produit,
        qte,
        ncProd
    };
  }

function createExcelFile(data, filePath) {
  const workbook = XLSX.utils.book_new();
  const worksheet = XLSX.utils.json_to_sheet(data);
  XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
  XLSX.writeFile(workbook, filePath);
}

function verifierSommeTotal(qte) {
   
    // Obtenir le prix affiché et le convertir en nombre
    cy.get('.sc-item-price-block').
    invoke('text')
    .then((prixText) => {
      const prixAffiche = parseFloat(prixText.replace(/[^0-9.-]+/g,"")); // Convertir le prix en nombr
      cy.log('la valeur prixAffiche est:'+ prixAffiche)
      const qteV = parseFloat(qte);
      const prixAfficheV = parseFloat(prixAffiche);

      const sousTotalAttendu = qteV * prixAfficheV; // Calculer le sous-total attendu
      cy.log('la valeur sousTotalAttendu est:'+ sousTotalAttendu)

      // Obtenir le sous-total affiché et le convertir en nombre
      cy.get('#sc-subtotal-amount-activecart .sc-price')
      .invoke('text')
      .then((subtotalText) => {
        const sousTotalAffiche = parseFloat(subtotalText.replace(/[^0-9.-]+/g,"")); // Convertir le sous-total en nombre
        cy.log('la valeur sousTotalAffiche est:' + sousTotalAffiche)
        //expect(sousTotalAffiche).to.equal(sousTotalAttendu); // Vérifier que le sous-total affiché est correct
      });
    });
  }

  function verifierProduit(nbr){
    cy.get('#sc-active-cart').then(cart => {
      const aria = cart.find('div[data-itemtype="active"]').length;
      cy.log(`Nombre de produits actifs à acheter : ${aria}`);
     // expect(aria).to.be.equal(nbr);
    });
  }
  function supprimerProduit(name) {
   
  //   cy.get('#sc-active-cart .sc-list-item').each(($el, index) => {
  //     // Vérifier le nom du produit
  //     cy.wrap($el).find('.sc-grid-item-product-title').invoke('text').then(productName => {
  //         if (productName.trim() !== name) {
  //             // Cliquer sur le bouton de suppression
  //             cy.get('#sc-active-cart .sc-list-item').eq(index).find('.sc-action-delete input[type="submit"]').then($deleteButton => {
  //                 if ($deleteButton.length > 0) {
  //                     cy.wrap($deleteButton).click().then(() => {
  //                         // Attendre que la suppression soit terminée avant de continuer
  //                         cy.wait(1000); // ou ajustez ce délai si nécessaire
  //                     });
  //                 } 
  //             });
  //         }
  //     });
  // });
  //   // Utiliser une boucle `each` pour parcourir les produits du panier
  //   cy.get('#sc-active-cart .sc-list-item').each(($el) => {
  //     // Vérifier le nom du produit
  //     cy.wrap($el).find('.sc-product-title').invoke('text').then((productName) => {
  //         if (productName.trim() !== name) {
  //             // Cliquer sur le bouton de suppression si le produit ne correspond pas au nom donné
  //             cy.wrap($el).find('[data-action="delete"] input[type="submit"]').click({ multiple: true,force: true });

  //             // Attendre une brève période pour que la suppression soit appliquée
  //             cy.wait(1000);
  //         }
  //     });
  // });
    }

  
  module.exports={
    getValueFromCell,
    getDataFromExcel,
    verifierSommeTotal,
    verifierProduit,
    supprimerProduit,
    createExcelFile
  }