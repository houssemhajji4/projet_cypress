// // ***********************************************
// // This example commands.js shows you how to
// // create various custom commands and overwrite
// // existing commands.
// //
// // For more comprehensive examples of custom
// // commands please read more here:
// // https://on.cypress.io/custom-commands
// // ***********************************************
// //
// //
// // -- This is a parent command --
// // Cypress.Commands.add('login', (email, password) => { ... })
// //
// //
// // -- This is a child command --
// // Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
// //
// //
// // -- This is a dual command --
// // Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
// //
// //
// // -- This will overwrite an existing command --
// // Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
// // cypress/support/commands.js

// const XLSX = require('xlsx');
// const fs = require('fs');

// Cypress.Commands.add('writeToExcel', (data) => {
//   // Création d'un nouveau classeur Excel
//   const wb = XLSX.utils.book_new();

//   // Création d'une feuille de calcul avec des données
//   const ws = XLSX.utils.aoa_to_sheet(data);

//   // Ajout de la feuille de calcul au classeur
//   XLSX.utils.book_append_sheet(wb, ws, 'Feuille 1');

//   // Conversion du classeur en flux de données binaires
//   const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'buffer' });

//   // Écriture du flux de données binaires dans un fichier
//   fs.writeFileSync('test.xlsx', excelBuffer);
// });
