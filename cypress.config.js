
const { defineConfig } = require("cypress");
const xlsx = require('xlsx');
const fs = require('fs');
const path = require('path');
//const { s2ab } = require('./cypress/support/Fonctions');

module.exports = defineConfig({
  e2e: {
    video: true,
    defaultCommandTimeout: 70000,
    testIsolation: false,
    setupNodeEvents(on, config) {
      on('task', {
        readExcelFile(filePath) {
          try {
            const absolutePath = path.resolve(filePath);
            const workbook = xlsx.readFile(absolutePath);
            const sheetName = workbook.SheetNames[0];
            const sheet = workbook.Sheets[sheetName];
            console.log('Sheet:', sheet);
            return { sheet, sheetName };
          } catch (error) {
            console.error('Error reading Excel file:', error);
            return null;
          }
        },

        writeExcel({ data, filePath }) {
          try {
            const absolutePath = path.resolve(filePath);
            const workbook = xlsx.readFile(absolutePath);
            const sheetName = workbook.SheetNames[0];
            const ws = xlsx.utils.aoa_to_sheet(data);

            if (!workbook.Sheets[sheetName]) {
              xlsx.utils.book_append_sheet(workbook, ws, sheetName);
            } else {
              workbook.Sheets[sheetName] = ws;
            }

            const wbout = xlsx.write(workbook, { bookType: 'xlsx', type: 'binary' });
            const buffer = Buffer.from(s2ab(wbout));
            fs.writeFileSync(absolutePath, buffer);
            return null;
          } catch (error) {
            console.error('Error writing Excel file:', error);
            return null;
          }
        },

        deleteFile(filePath) {
          return new Promise((resolve, reject) => {
            fs.unlink(filePath, (err) => {
              if (err) {
                reject(err);
              } else {
                resolve(null);
              }
            });
          });
        },

        checkFileExists(filepath) {
          return new Promise((resolve) => {
            resolve(fs.existsSync(filepath));
          });
        },

        createExcelFile({ data, filepath }) {
          try {
          const xlsx = require('xlsx');
          const fs = require('fs');
    
          // Créer un nouveau classeur
          const workbook = xlsx.utils.book_new();
    
          // Créer une nouvelle feuille en utilisant aoa_to_sheet pour éviter l'ajout de 0 et 1
          const worksheet = xlsx.utils.aoa_to_sheet(data);
    
          // Ajouter la feuille au classeur
          xlsx.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
      
          // Convertir le classeur en un binaire
          const wbout = xlsx.write(workbook, { bookType: 'xlsx', type: 'binary' });
    
          // Fonction utilitaire pour convertir le binaire en buffer
          const s2ab = (s) => {
            const buf = new ArrayBuffer(s.length);
            const view = new Uint8Array(buf);
            for (let i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
          };
    
          // Écrire le fichier
          const buffer = Buffer.from(s2ab(wbout), 'binary');
          fs.writeFileSync(filepath, buffer);
      
            return null; // Indique que le fichier a été créé avec succès
          } catch (error) {
            console.error('Error creating Excel file:', error);
            return error; // Retourne l'erreur si la création a échoué
          }
        }
    
       });
     }
  }
});
