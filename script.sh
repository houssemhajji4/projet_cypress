# show available browsers
npx cypress info
npx cypress run --reporter mochawesome --reporter-options "reportFilename=chrome-results" --browser chrome
#merge browser reports
npx mochawesome-merge ./mochawesome-report/*.json > all-browsers.json 
